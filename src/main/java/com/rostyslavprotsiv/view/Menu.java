package com.rostyslavprotsiv.view;

import java.util.Arrays;
import java.util.Scanner;

public class Menu {
    private static Menu instance;
    private static final Scanner SCANNER = new Scanner(System.in);
    public static final int ROW_LENGTH = 2;
    public static final String FORMATTING = "%-33s";

    private Menu() {}

    public static Menu getInstance() {
        if (instance == null) {
            instance = new Menu();
        }
        return instance;
    }

    public void welcome() {
        System.out.println("Welcome to my program!");
        System.out.println("What exactly would you like to do?");
        System.out.println("1. Test array");
        System.out.println("2. Test game");
    }

    public int inputOption() {
        return SCANNER.nextInt();
    }

    public void generatedArrays(Object[] first, Object[] second) {
        System.out.println("Generated these 2 arrays: ");
        System.out.println(Arrays.toString(first));
        System.out.println(Arrays.toString(second));
    }

    public void printArray(Object[] obj) {
        System.out.println(Arrays.toString(obj));
    }

    public void printRetainAll() {
        System.out.println("Retain all for first and second arrays");
    }

    public void printRemoveAll() {
        System.out.println("Remove all for first and second arrays");
    }

    public void printDeleteRepeatable() {
        System.out.println("Deleted repeatable elements for first array");
    }

    public void printDeleteInChain() {
        System.out.println("Deleted all elements in chain (except one)"
                + "  for first array");
    }

    public void welcomeToGame() {
        System.out.println("Hi, welcome to our game, but at first, you"
                + " need to fill info regarding hall");
    }

    public int chooseTheEntity() {
        System.out.println("To choose the entity, please, input: ");
        System.out.println("1. To choose Artifact ");
        System.out.println("2. To choose Monster");
        return SCANNER.nextInt();
    }

    public void congratulateAboutArtifact() {
        System.out.println("Congratulation! You chose the artifact to input");
    }

    public int inputArtifactPower(int minValue, int maxValue) {
        System.out.println("Please, input the artifact power in a range: ");
        System.out.println("Minimal value: " + minValue + " Maximum value: "
                + maxValue);
        return SCANNER.nextInt();
    }

    public String inputArtifactSize() {
        SCANNER.nextLine(); //eats enter key
        System.out.println("Please, input the artifact size");
        return SCANNER.nextLine();
    }

    public void congratulateAboutArtifactCreation() {
        System.out.println("Congratulation! You have created the artifact!");
    }

    public void congratulateAboutMonster() {
        System.out.println("Congratulation! You chose the monster to input");
    }

    public int inputMonsterPower(int minValue, int maxValue) {
        System.out.println("Please, input the monster power in a range: ");
        System.out.println("Minimal value: " + minValue + " Maximum value: "
                + maxValue);
        return SCANNER.nextInt();
    }

    public String inputMonsterName() {
        SCANNER.nextLine(); //eats enter key
        System.out.println("Please, input the monster name");
        return SCANNER.nextLine();
    }

    public void congratulateAboutMonsterCreation() {
        System.out.println("Congratulation! You have created the monster!");
    }

    public void outAllInfo(String objArr) {
        String objs = objArr.replaceAll(".*\\[", "");
        objs = objs.replaceAll("].*", "");
        objs = objs.replaceAll(" +", "");
        String doors[] = objs.split(",");
        for(int i = 1; i <= doors.length; i ++) {
            if(i % ROW_LENGTH == 0) {
                System.out.format("Door number: " + i + ") " + FORMATTING
                        + "\n", doors[i - 1]);
            } else {
                System.out.format("Door number: " + i + ") " + FORMATTING
                        + "| ", doors[i - 1]);
            }
        }
    }

    public void printNumberOfDoorsToBeDead(int n) {
        System.out.println("Number of doors to be dead: " + n);
    }

    public void printDoorOrderToStayAlive(int[] order) {
        if (order != null) {
            System.out.println("Order of doors to be alive"
                    + Arrays.toString(order));
        } else {
            System.out.println("Unfortunately, the are now ways to be alive");
        }
    }
}
