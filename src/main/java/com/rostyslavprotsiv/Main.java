package com.rostyslavprotsiv;

import com.rostyslavprotsiv.controller.Controller;
import com.rostyslavprotsiv.model.action.ArrayAction;
import com.rostyslavprotsiv.model.exception.DoorEntityLogicalException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class A { }

class B extends A{ }

class C extends B {}

public class Main {
    public static void main(String[] args) {
        Controller controller = new Controller();
        try {
            controller.execute();
        } catch(UnsupportedOperationException e) {
            e.printStackTrace();
        } catch (DoorEntityLogicalException e) {
            e.printStackTrace();
        }
//        Integer[] a = {3, 8, 8, 8, 8, 1, 1, 1, 5, 5, 4, 5, 1, 6};
//        Integer[] b = {5, 4, 1, 3, 3, 2, 1};
//        ArrayAction action = new ArrayAction();
//        A[] aa = new A[4];
//        B bb = new B();
//        C[] cc = new C[4];
//        cc = (C[]) aa;
//        System.out.println(a.retainAll(b));
//        System.out.println(a);
//        Object[] c = action.removeAll(a, b);
//        Object[] c = action.deleteRepeatable(a);
//        Object[] c = action.deleteInChain(a);
//        System.out.println(Arrays.toString(c));
    }
}
