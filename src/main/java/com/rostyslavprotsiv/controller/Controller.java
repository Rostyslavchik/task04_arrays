package com.rostyslavprotsiv.controller;

import com.rostyslavprotsiv.model.action.ArrayAction;
import com.rostyslavprotsiv.model.action.HallAction;
import com.rostyslavprotsiv.model.entity.Hall;
import com.rostyslavprotsiv.model.entity.doorentity.Artifact;
import com.rostyslavprotsiv.model.entity.doorentity.DoorEntity;
import com.rostyslavprotsiv.model.entity.doorentity.Monster;
import com.rostyslavprotsiv.model.exception.DoorEntityLogicalException;
import com.rostyslavprotsiv.model.generator.ArrayGenerator;
import com.rostyslavprotsiv.view.Menu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Controller {
    private static final Logger LOGGER = LogManager.getLogger(Controller.class);
    private Menu menu = Menu.getInstance();
    private ArrayAction arrayAction = new ArrayAction();
    private ArrayGenerator arrayGenerator = new ArrayGenerator();
    private HallAction hallAction = new HallAction();

    public void execute() throws UnsupportedOperationException,
            DoorEntityLogicalException {
        int option;
        menu.welcome();
        option = menu.inputOption();
        if (option == 1) {
            demonstrateArrayExample();
        } else if (option == 2) {
            Hall hall = new Hall();
            DoorEntity[] entities = hall.getEntities();
            menu.welcomeToGame();
            for(int i = 0; i < entities.length; i++) {
                entities[i] = createEntity();
            }
            menu.outAllInfo(hall.toString());
            outNumOfDoorsToBeDead(hall);
            outDoorOrderToStayAlive(hall);
        } else {
            LOGGER.error("There is no such option");
            LOGGER.warn("There is no such option");
            throw new UnsupportedOperationException("There is no such option"
                    + " within this software");
        }
    }

    private void demonstrateArrayExample() {
        Number[] first = arrayGenerator.generateFirst();
        Object[] second = arrayGenerator.generateSecond();
        menu.generatedArrays(first, second);
        demonstrateRetainAll(first, second);
        demonstrateRemoveAll(first, second);
        demonstrateDeleteRepeatable(first);
        demonstrateDeleteInChain(first);
    }

    private void demonstrateRetainAll(Object[] first, Object[] second) {
        menu.printRetainAll();
        Object[] result = arrayAction.retainAll(first, second);
        menu.printArray(result);
    }

    private void demonstrateRemoveAll(Object[] first, Object[] second) {
        menu.printRemoveAll();
        Object[] result = arrayAction.removeAll(first, second);
        menu.printArray(result);
    }

    private void demonstrateDeleteRepeatable(Number[] first) {
        menu.printDeleteRepeatable();
        Object[] result = arrayAction.deleteRepeatable(first);
        menu.printArray(result);
    }

    private void demonstrateDeleteInChain(Object[] first) {
        menu.printDeleteInChain();
        Object[] result = arrayAction.deleteInChain(first);
        menu.printArray(result);
    }

    private DoorEntity createEntity() throws DoorEntityLogicalException {
        int entityOption = menu.chooseTheEntity();
        int power;
            if (entityOption == 1) {        //artifact was chosen
                menu.congratulateAboutArtifact();
                power = menu.inputArtifactPower(Artifact.MIN_POWER,
                        Artifact.MAX_POWER);
                String size = menu.inputArtifactSize();
                menu.congratulateAboutArtifactCreation();
                return new Artifact(power, size);
            } else if (entityOption == 2) {
                menu.congratulateAboutMonster();
                power = menu.inputMonsterPower(Monster.MIN_POWER,
                        Monster.MAX_POWER);
                String name = menu.inputMonsterName();
                menu.congratulateAboutMonsterCreation();
                return new Monster(power, name);
            } else {
                LOGGER.error("There is no such option for DoorEntity");
                LOGGER.warn("There is no such option for DoorEntity");
                throw new UnsupportedOperationException("There is no such option"
                        + " within this software for DoorEntity");
            }
    }

    private void outNumOfDoorsToBeDead(Hall hall) {
        int n = hallAction.getNumberOfDoorsToBeDead(0, 0, hall);
        menu.printNumberOfDoorsToBeDead(n);
    }

    private void outDoorOrderToStayAlive(Hall hall) {
        int[] order = hallAction.getDoorOrderToStayAlive(hall);
        menu.printDoorOrderToStayAlive(order);
    }
}
