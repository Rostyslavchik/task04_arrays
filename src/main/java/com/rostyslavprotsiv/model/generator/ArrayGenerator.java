package com.rostyslavprotsiv.model.generator;

public class ArrayGenerator {

    public Integer[] generateFirst() {
        return new Integer[]{3, 8, 8, 8, 8, 1, 1, 1, 5, 5, 4, 5, 1, 6};
    }

    public Integer[] generateSecond() {
        return new Integer[]{5, 4, 1, 3, 3, 2, 1};
    }
}
