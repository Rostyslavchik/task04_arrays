package com.rostyslavprotsiv.model.entity;

import com.rostyslavprotsiv.model.entity.doorentity.DoorEntity;

import java.util.Arrays;
import java.util.Objects;

public class Hall {
    public static final int DOOR_NUMBER = 10;
    private DoorEntity[] entities = new DoorEntity[DOOR_NUMBER];
    private Hero hero;

    public Hall() {
        hero = new Hero();
    }

    public Hall(DoorEntity[] entities, Hero hero) {
        this.entities = entities;
        this.hero = hero;
    }

    public DoorEntity[] getEntities() {
        return entities;
    }

    public void setEntities(DoorEntity[] entities) {
        this.entities = entities;
    }

    public Hero getHero() {
        return hero;
    }

    public void setHero(Hero hero) {
        this.hero = hero;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hall hall = (Hall) o;
        return Arrays.equals(entities, hall.entities) && Objects.equals(hero, hall.hero);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(hero);
        result = 31 * result + Arrays.hashCode(entities);
        return result;
    }

    @Override
    public String toString() {
        return "Hall{" +
                "entities=" + Arrays.toString(entities) +
                ", hero=" + hero +
                '}';
    }
}
