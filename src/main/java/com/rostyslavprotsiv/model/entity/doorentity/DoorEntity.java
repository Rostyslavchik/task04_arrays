package com.rostyslavprotsiv.model.entity.doorentity;

import com.rostyslavprotsiv.model.exception.DoorEntityLogicalException;

import java.util.Objects;

public abstract class DoorEntity {
    protected int power;

    public DoorEntity() {}

    public DoorEntity(int power){
            this.power = power;
    }

    public int getPower() {
        return power;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DoorEntity that = (DoorEntity) o;
        return power == that.power;
    }

    @Override
    public int hashCode() {
        return Objects.hash(power);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + '@'
                + " power = " + power;
    }
}
