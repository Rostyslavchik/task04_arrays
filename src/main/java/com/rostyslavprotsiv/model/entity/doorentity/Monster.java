package com.rostyslavprotsiv.model.entity.doorentity;

import com.rostyslavprotsiv.model.exception.ArtifactLogicalException;
import com.rostyslavprotsiv.model.exception.DoorEntityLogicalException;
import com.rostyslavprotsiv.model.exception.MonsterLogicalException;

import java.util.Objects;

public class Monster extends DoorEntity {
    public static final int MIN_POWER = -100;
    public static final int MAX_POWER = -5;
    private String name;

    public Monster() {}

    public Monster(int power, String name) throws MonsterLogicalException {
        if (checkPower(power)) {
            this.power = power;
            this.name = name;
        } else {
            throw new MonsterLogicalException("Something is bad with "
                    + "power boundaries");
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPower(int power) throws MonsterLogicalException {
        if (checkPower(power)) {
            this.power = power;
        } else {
            throw new MonsterLogicalException("Something is bad with "
                    + "power boundaries");
        }
    }

    public boolean checkPower(int power) {
        return (power > MIN_POWER && power < MAX_POWER);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Monster monster = (Monster) o;
        return Objects.equals(name, monster.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), name);
    }

    @Override
    public String toString() {
        return super.toString() + " name='" + name + '\'';
    }
}
