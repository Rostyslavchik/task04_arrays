package com.rostyslavprotsiv.model.entity.doorentity;

import com.rostyslavprotsiv.model.exception.ArtifactLogicalException;
import com.rostyslavprotsiv.model.exception.DoorEntityLogicalException;
import com.rostyslavprotsiv.model.exception.MonsterLogicalException;

import java.util.Objects;

public class Artifact extends DoorEntity {
    public static final int MIN_POWER = 10;
    public static final int MAX_POWER = 80;
    private String size;

    public Artifact() {}

    public Artifact(int power, String size) throws ArtifactLogicalException {
        if (checkPower(power)) {
            this.power = power;
            this.size = size;
        } else {
            throw new ArtifactLogicalException("Something is bad with "
                    + "power boundaries");
        }
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public void setPower(int power) throws ArtifactLogicalException {
        if (checkPower(power)) {
            this.power = power;
        } else {
            throw new ArtifactLogicalException("Something is bad with "
                    + "power boundaries");
        }
    }

    public boolean checkPower(int power) {
        return (power > MIN_POWER && power < MAX_POWER);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Artifact artifact = (Artifact) o;
        return Objects.equals(size, artifact.size);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), size);
    }

    @Override
    public String toString() {
        return super.toString() + " size='" + size + '\'';
    }
}
