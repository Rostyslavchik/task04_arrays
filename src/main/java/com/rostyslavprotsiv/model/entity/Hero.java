package com.rostyslavprotsiv.model.entity;

import java.util.Objects;

public class Hero {
    private String name;
    private int initPower = 25;

    public Hero() {}

    public Hero(String name, int initPower) {
        this.name = name;
        this.initPower = initPower;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getInitPower() {
        return initPower;
    }

    public void setInitPower(int initPower) {
        this.initPower = initPower;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hero hero = (Hero) o;
        return initPower == hero.initPower && Objects.equals(name, hero.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, initPower);
    }

    @Override
    public String toString() {
        return "Hero{" + "name='" + name + '\''
                + ", initPower=" + initPower + '}';
    }
}
