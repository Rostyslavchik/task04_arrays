package com.rostyslavprotsiv.model.exception;

public class ArtifactLogicalException extends DoorEntityLogicalException {
    public ArtifactLogicalException() {
    }

    public ArtifactLogicalException(String s) {
        super(s);
    }

    public ArtifactLogicalException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public ArtifactLogicalException(Throwable throwable) {
        super(throwable);
    }
}
