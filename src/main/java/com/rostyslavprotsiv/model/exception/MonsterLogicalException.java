package com.rostyslavprotsiv.model.exception;

public class MonsterLogicalException extends DoorEntityLogicalException {
    public MonsterLogicalException() {
    }

    public MonsterLogicalException(String s) {
        super(s);
    }

    public MonsterLogicalException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public MonsterLogicalException(Throwable throwable) {
        super(throwable);
    }
}
