package com.rostyslavprotsiv.model.exception;

public class DoorEntityLogicalException extends Exception {
    public DoorEntityLogicalException() {
    }

    public DoorEntityLogicalException(String s) {
        super(s);
    }

    public DoorEntityLogicalException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public DoorEntityLogicalException(Throwable throwable) {
        super(throwable);
    }
}
