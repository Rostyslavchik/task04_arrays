package com.rostyslavprotsiv.model.action;

public class ArrayAction {
    public static final int MAX_OCCURRENCES = 2;

    public <T> Object[] deleteInChain(T[] arr) {
        Object[] result = new Object[0];
        for (int i = 0; i < arr.length - 1; i++) {
            if (!arr[i].equals(arr[i + 1])) {
                result = addAndGet(result, arr[i]);
            }
        }
        if (!arr[arr.length - 2].equals(arr[arr.length - 1])) {
            result = addAndGet(result, arr[arr.length - 1]);
        }
        return result;
    }

    public <T extends Number> Object[] deleteRepeatable(T[] arr) {
        Object[] result = new Object[0];
        int occurrences = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                if (arr[i].equals(arr[j])) {
                    occurrences ++;
                }
            }
            if (occurrences <= MAX_OCCURRENCES) {
                result = addAndGet(result, arr[i]);
            }
            occurrences = 0;
        }
        return result;
    }

    public <T> Object[] retainAll(T[] first, T[] second) {
        Object[] result = new Object[0];
        int elementNumInFirst;
        int elementNumInSecond;
        for (int i = 0; i < first.length; i++) {
            elementNumInFirst = 0;
            elementNumInSecond = 0;
            if (!contains(result, first[i])) {
                for (int j = 0; j < first.length; j++) {
                    if (first[i].equals(first[j])) {
                        elementNumInFirst++;
                    }
                }
                for (int j = 0; j < second.length; j++) {
                    if (first[i].equals(second[j])) {
                        elementNumInSecond++;
                    }
                }
                for (int j = 0; j < getSmallerInt(elementNumInFirst,
                        elementNumInSecond); j++) {
                    result = addAndGet(result, first[i]);
                }
            }
        }
        return result;
    }

    public <T> Object[] removeAll(T[] first, T[] second) {
        Object[] resultFirstRemoving = removeFromFirst(first, second);
        return removeFromSecond(resultFirstRemoving, first, second);
    }

    private <T> Object[] removeFromFirst(T[] first, T[] second) {
        Object[] result = new Object[0];
        int elementNumInFirst;
        int elementNumInSecond;
        int difference;
        for (int i = 0; i < first.length; i++) {
            elementNumInFirst = 0;
            elementNumInSecond = 0;
            if (!contains(result, first[i])) {
                for (int j = 0; j < first.length; j++) {
                    if (first[i].equals(first[j])) {
                        elementNumInFirst++;
                    }
                }
                for (int j = 0; j < second.length; j++) {
                    if (first[i].equals(second[j])) {
                        elementNumInSecond++;
                    }
                }
                difference = elementNumInFirst - elementNumInSecond;
                for (int j = 0; j < difference; j++) {
                    result = addAndGet(result, first[i]);
                }
            }
        }
        return result;
    }

    private <T> Object[] removeFromSecond(Object[] result,T[] first, T[] second) {
        int elementNumInFirst;
        int elementNumInSecond;
        int difference;
        for (int i = 0; i < second.length; i++) {
            elementNumInFirst = 0;
            elementNumInSecond = 0;
            if (!contains(result, second[i])) {
                for (int j = 0; j < first.length; j++) {
                    if (second[i].equals(first[j])) {
                        elementNumInFirst++;
                    }
                }
                for (int j = 0; j < second.length; j++) {
                    if (second[i].equals(second[j])) {
                        elementNumInSecond++;
                    }
                }
                difference = elementNumInSecond - elementNumInFirst;
                for (int j = 0; j < difference; j++) {
                    result = addAndGet(result, second[i]);
                }
            }
        }
        return result;
    }

    private <T> boolean contains(T[] arr, T elem) {
        if (arr != null) {
            for (T el : arr) {
                if (el != null) {
                    if (el.equals(elem)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private int getBiggerInt(int a, int b) {
        int bigger = 0;
        if (a > b) {
            bigger = a;
        } else {
            bigger = b;
        }
        return bigger;
    }

    private int getSmallerInt(int a, int b) {
        int smaller = 0;
        if (a < b) {
            smaller = a;
        } else {
            smaller = b;
        }
        return smaller;
    }

    private <T> Object[] addAndGet(T[] arr, T elem) {
        Object[] result = new Object[arr.length + 1];
        for (int i = 0; i < arr.length; i++) {
                result[i] = arr[i];
        }
        result[arr.length] = elem;
        return result;
    }
}
