package com.rostyslavprotsiv.model.action;

import com.rostyslavprotsiv.model.entity.Hall;
import com.rostyslavprotsiv.model.entity.Hero;
import com.rostyslavprotsiv.model.entity.doorentity.DoorEntity;

public class HallAction {
    public int getNumberOfDoorsToBeDead(int doorIndex,
                                            int numOfDoors, Hall hall) {
        if (doorIndex == hall.getEntities().length) {
            return numOfDoors;
        }
        if ((hall.getHero().getInitPower()
                + hall.getEntities()[doorIndex].getPower()) <= 0) {
            numOfDoors = getNumberOfDoorsToBeDead(doorIndex + 1,
                    numOfDoors + 1, hall);
        } else {
            numOfDoors = getNumberOfDoorsToBeDead(doorIndex + 1,
                    numOfDoors, hall);
        }
        return numOfDoors;
    }

    public int[] getDoorOrderToStayAlive(Hall hall) {
        DoorEntity[] entities = hall.getEntities();
        int[] order = null;
        if (canBeAlive(hall)) {
            order = new int[entities.length];
            for (int i = 0; i < entities.length; i++) {
                if (entities[i].getPower() > 0) {
                    addToStart(order, i + 1);
                } else {
                    addToEnd(order, i + 1);
                }
            }
        }
        return order;
    }

    private void addToStart(int[] arr, int elem) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 0) {
                arr[i] = elem;
                return;
            }
        }
    }

    private void addToEnd(int[] arr, int elem) {
        for (int i = arr.length - 1; i >=0 ; i--) {
            if (arr[i] == 0) {
                arr[i] = elem;
                return;
            }
        }
    }

    private boolean canBeAlive(Hall hall) {
        DoorEntity[] entities = hall.getEntities();
        int sumPower = hall.getHero().getInitPower();
        for (int i = 0; i < entities.length; i++) {
            sumPower += entities[i].getPower();
        }
        return sumPower > 0;
    }
}
